#
# Executes commands at the start of an interactive session.
#
# Authors:
#   Sorin Ionescu <sorin.ionescu@gmail.com>
#

# Source Prezto.
if [[ -s "${ZDOTDIR:-$HOME}/.zprezto/init.zsh" ]]; then
  source "${ZDOTDIR:-$HOME}/.zprezto/init.zsh"
fi

# Customize to your needs...

# Overrides Utility Grep alias Grep
if zstyle -t ':prezto:module:utility:grep' color; then
  export GREP_COLOR='38;5;202'           # BSD.
  export GREP_COLORS="mt=$GREP_COLOR" # GNU.
  alias grep="${aliases[grep]:-grep} --color=auto"
fi

export TERM=xterm-256color
export EDITOR="/Applications/TextMate.app/Contents/MacOS/mate"
export VISUAL="/Applications/TextMate.app/Contents/MacOS/mate"

# Damatically improves rails asset compilation in development mode
export EXECJS_RUNTIME='Node'

export PATH=/usr/local/bin:~/bin:$PATH

[[ -s "$HOME/.profile" ]] && source "$HOME/.profile" # Load the default .profile'
[[ -s "$HOME/.rvm/scripts/rvm" ]] && source "$HOME/.rvm/scripts/rvm" # Load RVM into a shell session *as a function*
[[ -s "$HOME/.zshrc_local" ]] && source "$HOME/.zshrc_local" # Load user private .zshrc

# Add eazyBI log file analyzer to the path
export PATH="/Users/janisvitums/rails/eazybi_support/log_analyzer:$PATH"

# The next line updates PATH for the Google Cloud SDK.
if [ -f /Users/janisvitums/bin/google-cloud-sdk/path.zsh.inc ]; then
  source '/Users/janisvitums/bin/google-cloud-sdk/path.zsh.inc'
fi

# The next line enables shell command completion for gcloud.
if [ -f /Users/janisvitums/bin/google-cloud-sdk/completion.zsh.inc ]; then
  source '/Users/janisvitums/bin/google-cloud-sdk/completion.zsh.inc'
fi

export PATH="/usr/local/opt/postgresql@9.6/bin:$PATH"
export PKG_CONFIG_PATH="/usr/local/opt/postgresql@9.6/lib/pkgconfig"

#  WINE for winbox for mikrotik routers
# export PATH="/Applications/Wine Staging.app/Contents/Resources/wine/bin:$PATH"
export FREETYPE_PROPERTIES="truetype:interpreter-version=35"
export DYLD_FALLBACK_LIBRARY_PATH="/usr/lib:/opt/X11/lib:$DYLD_FALLBACK_LIBRARY_PATH"

# Add RVM to PATH for scripting. Make sure this is the last PATH variable change.
